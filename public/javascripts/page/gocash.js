$(function(){
    window.onresize=resizeBannerImage;//当窗口改变宽度时执行此函数
    window.onload=resizeBannerImage;
    function resizeBannerImage()
       {
        var winW = $(window).width();
        $('.slides li').outerWidth(winW); 
        w = $('.slides li').outerWidth();
        $('#slider ul').width(w*len+w);
        $('#slider ul li,#slider ul li img').width(w);   
    }
    //banner轮播
    var len = $('.slides li').size(),
        ind = 0,
        w = $('.slides li').outerWidth(),
        timer = null;
        $('#slider ul').width(w*len+w);
        $('#slider ul li,#slider ul li img').width(w);
    $('.right').on('click',function(){
        next();
        change();
    });
    $('.left').on('click',function(){
        prev();
        change();
    });
    function prev(){
        ind--;
        if(ind<0){
            $('#slides li:last').clone().prependTo('ul');
            $('#slides').css('marginLeft',-w);
            $('#slides').stop().animate({'marginLeft':0},500,function(){
                $('#slides li:first').remove();
                $('#slides').css({'marginLeft':-w*ind});
            });
            ind = len-1;   
        };
        change();
    }
    function change(){
        $('#slider .slides').stop().animate({'marginLeft':-ind*w},500);
    };
    function auto(){
        timer = setInterval(function(){
            prev();
            change(ind);
        },2000);
    };
    auto();
    function next(){
        ind++;
        if(ind>len-1){
            $('#slides li:first').clone().appendTo('ul');
            $('#slides').stop().animate({'marginLeft':-w*ind},500,function(){
                $('#slides li:last').remove();
                $('#slides').css('marginLeft',0);
            });
            ind=0;   
        };   
    };
    $('#slider').hover(function(){
        clearInterval(timer);
    },function(){
        auto();
    });

    //滑过header加下划线
    function slide(checker, liner) {
        $(checker).mouseover(function () {
            var _index = $(this).index();
            var _length = $(this).width();
            $(checker).siblings(liner).stop(true, false).animate({ marginLeft: 0+'px' }, 300);
        	var l=_index*_length+"px";
        	$(liner).css({width:_length});
        	if(_index==1){
        		$(checker).siblings(liner).stop(true, false).animate({ marginLeft: l }, 300);
        	};
        	if(_index==2){
        		$(checker).siblings(liner).stop(true, false).animate({ marginLeft: l }, 300);
        	};
        	if(_index==3){
        		$(checker).siblings(liner).stop(true, false).animate({ marginLeft: l }, 300);
            };
            if(_index==4){
        		$(checker).siblings(liner).stop(true, false).animate({ marginLeft: l }, 300);
        	};
        });
        
    };
    slide('.tab-con', '.tab .liner');

    $(document).scroll(function(){
            if($(document).scrollTop()>0 && $(document).scrollTop()<30){
                $(".liner").stop().animate({ width: 150+'px' , marginLeft: 0 +'px'},300);
            }
            if($(document).scrollTop()>630 && $(document).scrollTop()<660){
                $(".liner").stop().animate({ width: 150+'px' , marginLeft: 165 +'px'},300);
            }
            if($(document).scrollTop()>1860 && $(document).scrollTop()<1890){
                $(".liner").stop().animate({ width: 150+'px' , marginLeft: 325 +'px'},300);
            }
            if($(document).scrollTop()>2000 && $(document).scrollTop()<2530){
                $(".liner").stop().animate({ width: 100+'px' , marginLeft: 670 +'px'},300);
            }
       });

    //滑到一定高度出现动画
    $(document).scroll(function(){
    	if($(document).scrollTop()>450){
            $('.animate-pic').animate({opacity: 1}).animate({marginTop: '0%'},500);
	    	setTimeout(function(){
                $('.animate-con').animate({opacity: 1}).animate({marginTop: '0%'},500);
            });
            setTimeout(function(){
               $('.animate-text').animate({opacity: 1}).animate({marginTop: '0%'},500);
            }); 
	    };
        if($(document).scrollTop()>1000){
            $('.feature-title').animate({opacity: 1}).animate({paddingTop: '74px'},500);
            setTimeout(function(){
                $('.feature dl dt').animate({opacity: 1}).animate({marginTop: '-20px'},500);
            });
            setTimeout(function(){
                $('.feature dl dd h2').animate({opacity: 1}).animate({marginTop:'-160px'},500);
            });
            setTimeout(function(){
                $('.feature dl dd p').animate({opacity: 1}).animate({marginTop: '30px'},500);
            });
        };
    });

    // Change header background color when navbar-collapse.show
    $('.navbar-toggler').on('click', function() {
        if(!$('#navbarNavDropdown').hasClass('show')) {
            $('#navbarNavDropdown').toggle(true);        
            $('.navbar').addClass('green-back').fadeIn(2000);
        } else {
            $('#navbarNavDropdown').toggle(false);        
        }
    });

    
    //滑过步骤切换图片
    $('.steps  li').eq(0).find('b').show();
    for(var i=0;i<$('.steps li').length;i++){
        $(this).index = i;
        $('.steps li').mouseover(function(){
            for(var i=0;i<$('.steps li').length; i++){
                $('.steps li').removeClass('first');
                $('.steps li').find('b').hide();
                $('.steps li').addClass('font-colors').removeClass('font-color');
            };
            $(this).addClass('first');
            $(this).removeClass('font-colors').addClass('font-color');
            $(this).find('b').show();
        });
    };

    $('.steps li').hover(function () {
        var index = $(this).index();
        $('.move-pic').stop().animate({'marginLeft':-256 * index+'px'},500);
        $('.phone-right-content').stop().animate({'marginLeft': -330 * index +'px'},500);
    });

    $(window).on('scroll', function (e) {
        var scrollHeight = $(document).scrollTop();
        if(scrollHeight == 0){
            $('.navbar').removeClass('green-back').fadeIn(2000);
        }else{
            $('.navbar').addClass('green-back').fadeIn(1000);
            $('#navbarNavDropdown').toggle(false);
        };
    });

    //返回顶部
    $('.goto').click(function(){
       $('body,html').animate({scrollTop:0},1000);
    });

    //鼠标滑过图片变大图
    $('.imgs-one').hover(function(){
        $(this).attr('src','public/stylesheets/img/bigone.png');
    },function(){
        $(this).attr('src','public/stylesheets/img/bottomone.png');
    });
    $('.imgs-two').hover(function(){
        $(this).attr('src','public/stylesheets/img/bigtwo.png');
    },function(){
        $(this).attr('src','public/stylesheets/img/bottomtwo.png');
    });
    $('.imgs-three').hover(function(){
        $(this).attr('src','public/stylesheets/img/bigthree.png');
    },function(){
        $(this).attr('src','public/stylesheets/img/bottomthree.png');
    });
    $('.imgs-four').hover(function(){
        $(this).attr('src','public/stylesheets/img/bigfour.png');
    },function(){
        $(this).attr('src','public/stylesheets/img/bottomfour.png');
    });

    //点击header的a标签，页面滚动到相应的位置
    var headerHeight = $('.header').height();
    $("a").each(function () {
        $(this).click(function(){
            var href = $(this).attr('href'),
            height = $(href).offset().top;
            $('html,body').animate({
                scrollTop: height - headerHeight
            }, 500);
        });
    });
});